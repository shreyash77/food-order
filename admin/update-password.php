<?php include('./partials/menu.php'); ?>
<?php include('./master.php'); ?>

<div class="main-content">
    <div class="wrapper">
        <h1>
            Change Password
        </h1>
        <br>
        <?php
            if(isset($_GET['id'])){
                $id = $_GET['id'];
            }
        ?>

        <?php
            $errMsg = "";
            if(isset($_POST['submit'])){
                $id = $_POST['id'];
                $current_password = md5($_POST['current_password']);
                $new_password = md5($_POST['new_password']);
                $confirm_password = md5($_POST['confirm_password']);
                $sql = "SELECT * FROM tbl_admin WHERE id=$id AND password='$current_password';";
                $res = mysqli_query($conn, $sql);

                if($res == true){
                    $count = mysqli_num_rows($res);
                    if($count == 1){
                        if($new_password == $confirm_password){
                            $sql2 = "UPDATE tbl_admin set
                                password='$new_password'
                                WHERE id='$id'
                            ";
                            $res2 = mysqli_query($conn, $sql2);
                            setAdminStatus("Password Change", "$res2");
                        }else{
                            $errMsg = "Confirm Password Didn't match";
                        }
                    }else{
                        $errMsg = "Enter Old Password Correctly";
                    }
                }
            }
        ?>

        <form action="" method="POST">
            <table class="tbl-30">
                <tr>
                    <?php 
                    echo "<div class='error-msg'>". $errMsg . "</div>"
                    ?>
                </tr>
                <tr>
                    <td>
                        Currrent Password
                    </td>
                    <td>
                        <input type="password" name="current_password" value="" placeholder="Old Password">
                    </td>
                </tr>
                <tr>
                    <td>
                        New Password
                    </td>
                    <td>
                        <input type="password" name="new_password" value="" placeholder="New Password">
                    </td>
                </tr>
                <tr>
                    <td>
                        confirm Password
                    </td>
                    <td>
                        <input type="password" name="confirm_password" value="" placeholder="Confirm Password">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                        <input type="submit" name="submit" class="btn-secondary">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<?php include('./partials/footer.php'); ?>