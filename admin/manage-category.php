<?php include('partials/menu.php'); ?>

<div class="main-content">
    <div class="wrapper">
        <h1>
            Manage Category
        </h1>
        <br><br>
        <!-- button to add admin -->
        <a href="#" class="btn-primary text-deco">Add Category</a>
        <br><br>
        <table class="tbl-full">
            <tr>
                <th>S.N</th>
                <th>Full Name</th>
                <th>User Name</th>
                <th>Action</th>
            </tr>
            <tr>
                <td>1.</td>
                <td>Shreyash Shrestha</td>
                <td>Khapper</td>
                <td>
                    <a href="#" class="btn-secondary text-deco">Update Category</a>
                    <a href="#" class="btn-danger text-deco">Delete Category</a> 
                </td>
            </tr>
            <tr>
                <td>2.</td>
                <td>Shreyash Shrestha</td>
                <td>Khapper</td>
                <td>
                    <a href="#" class="btn-secondary text-deco">Update Category</a>
                    <a href="#" class="btn-danger text-deco">Delete Category</a>
                </td>
            </tr>
            <tr>
                <td>3.</td>
                <td>Shreyash Shrestha</td>
                <td>Khapper</td>
                <td>
                    <a href="#" class="btn-secondary text-deco">Update Category</a>
                    <a href="#" class="btn-danger text-deco">Delete Category</a>
                </td>
            </tr>
            <tr>
                <td>4.</td>
                <td>Shreyash Shrestha</td>
                <td>Khapper</td>
                <td>
                    <a href="#" class="btn-secondary text-deco">Update Category</a>
                    <a href="#" class="btn-danger text-deco">Delete Category</a>
                </td>
            </tr>
        </table>
    </div>

</div>

<?php
    include('partials/footer.php');
?>
