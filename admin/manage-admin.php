<?php include('partials/menu.php') ?>
    <!-- main section start -->
    <div class="main-content">
        <div class="wrapper">
            <h1>Managa Admin</h1>
            <br>
        <!-- displaying message using session -->
            <?php 
                function giveStatus():void
                {
                    if(isset($_SESSION["admin_status"])){
                        echo "<br>";
                        echo $_SESSION["admin_status"];
                        echo "<br><br>";
                        unset($_SESSION["admin_status"]);
                    }
                }
                giveStatus();
            ?>
            <br>
            <!-- button to add admin -->
            <a href="add-admin.php" class="btn-primary text-deco">Add Admin</a>
            <br><br>
            <table class="tbl-full">
                <tr>
                    <th>S.N</th>
                    <th>Full Name</th>
                    <th>User Name</th>
                    <th>Action</th>
                </tr>
                <?php
                    $sql = "SELECT * FROM tbl_admin;";
                    $res = mysqli_query($conn, $sql);
                    $sn = 1;

                    if($res == true){
                        // count rows to check whether we have data in database or not
                        $rows = mysqli_num_rows($res);
                        if($rows > 0){
                            // we have data in database
                            // using while loop to get all the data from database
                            while($rows = mysqli_fetch_assoc($res)){
                                $id = $rows['id'];
                                $full_name = $rows['full_name'];
                                $user_name = $rows['user_name'];
                                ?>
                                <tr>
                                    <td><?php echo $sn++ ?></td>
                                    <td><?php echo $full_name ?></td>
                                    <td><?php echo $user_name ?></td>
                                    <td>
                                        <a href='<?php echo SITEURL ?>admin/update-password.php?id=<?php echo $id ?>' class="btn-primary text-deco">Change Password</a>
                                        <a href='<?php echo SITEURL ?>admin/update-admin.php?id=<?php echo $id ?>' class='btn-secondary text-deco'>Update Admin</a>
                                        <a href='<?php echo SITEURL ?>admin/delete-admin.php?id=<?php echo $id ?>' class='btn-danger text-deco'>Delete Admin</a>
                                    </td>
                                </tr>
                                <?php
                            }
                        }else{
                            // don't have data ne database
                        }
                    }
                ?>
            </table>
        </div>
    </div>
    <!-- main section end -->
<?php include('partials/footer.php') ?>