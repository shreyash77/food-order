<?php include('./partials/menu.php')?>
<?php include('./master.php'); ?>

<div class="main-content">
    <div class="wrapper">
        <h1>Add Admin</h1>        
        <br>
        <?php
            if(isset($_SESSION['admin_status'])){
                echo "<br>";
                echo $_SESSION['admin_status'];
                echo "<br><br>";
                unset($_SESSION['admin_status']);
            }
        ?>
        <form action="../app/add_admin.php" method="POST">
            <table class="tbl-30">
                <tr>
                    <td>
                        Full Name
                    </td>
                    <td>
                        <input type="text" name="full_name" placeholder="Enter your name">
                    </td>
                </tr>
                <tr>
                    <td>
                        User Name
                    </td>
                    <td>
                        <input type="text" name="username" placeholder="Enter your user name">
                    </td>
                </tr>
                <tr>
                    <td>
                        Password
                    </td>
                    <td>
                        <input type="password" name="password" placeholder="Enter your password">
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" name="submit" class="btn-secondary">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<?php include('partials/footer.php'); ?>