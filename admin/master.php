<?php
    function setAdminStatus($status, $res): void
    {
        if($res == true){
            if($status == "Add"){
                $_SESSION["admin_status"] = "<strong class='success-msg'> $status Successfull </strong>";
            }elseif($status == "Delete"){
                $_SESSION["admin_status"] = "<strong class='error-msg'> $status Successfull </strong>";
            }elseif($status == "Update"){
                $_SESSION["admin_status"] = "<strong class='success-msg'> $status Successfull </strong>";
            }elseif($status == "Password Change"){
                $_SESSION["admin_status"] = "<strong class='success-msg'> $status Successfull </strong>";
            }elseif($status == "User Not Found"){
                $_SESSION["admin_status"] = "<strong class='success-msg'> $status Successfull </strong>";
            }
        }elseif($status == "Enter same password"){
            $_SESSION["admin_status"] = "<strong class='error-msg'> $status </strong>";
        }else{
            $_SESSION["admin_status"] = "<strong class='error-msg'> $status Unsuccessfull </strong>";
        }
        header("location:".SITEURL."admin/manage-admin.php");
    }

    function getAdminData($id, $conn):array|string
    {
        $sql = "SELECT * FROM tbl_admin WHERE id=$id";
        $res = mysqli_query($conn, $sql);

        if($res == true){
            $count = mysqli_num_rows($res);
            if($count == 1){
                $row = mysqli_fetch_assoc($res);
                return $row;
            }else{
                // redirect to manage admin page
                header("location:". SITEURL . "admin/manage-admin.php");
            }
        }
    }