<?php include('../config/constant.php');  ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Food order - Home Page</title>
    <link rel="stylesheet" href="../css/admin.css">
</head>
<body>
    <!-- <h1>Admin Panal</h1> -->


    <!-- menu section star -->

    <div class="menu text-center">
        <div class="wrapper">
            <ul>
                <li>
                    <a class="text-deco" href="index.php">Home</a>
                </li>
                <li>
                    <a class="text-deco" href="manage-admin.php">Admin</a>
                </li>
                <li>
                    <a class="text-deco" href="manage-category.php">Category</a>
                </li>
                <li>
                    <a class="text-deco" href="manage-food.php">Food</a>
                </li>
                <li>
                    <a class="text-deco" href="manage-order.php">Order</a>
                </li>
                
            </ul>
        </div>
    </div>

    <!-- menu section end -->