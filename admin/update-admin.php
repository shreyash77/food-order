<?php include('./partials/menu.php'); ?>
<?php include('./master.php'); ?>

<div class="main-content">
    <div class="wrapper"> 
        <h1>Update Admin</h1>
        <br><br>
        <?php
            $id = $_GET['id'];
            $row = getAdminData($id, $conn);
        ?>

        <form action="../app/update_admin.php" method="POST">
            <table class="tbl-30">
                <tr>
                    <td>
                        Full Name
                    </td>
                    <td>
                        <input type="text" name="full_name" value="<?php echo $row["full_name"] ?>">
                    </td>
                </tr>
                <tr>
                    <td>
                        User Name
                    </td>
                    <td>
                        <input type="text" name="user_name" value="<?php echo $row["user_name"] ?>">
                    </td>
                </tr>
                <tr>
                    <td colspan="2"> 
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                        <input type="submit" name="submit" value="update Admin" class="btn-secondary">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<?php include('./partials/footer.php');